var BotHelper = require('../Functions/BotFunctions');
var menu = [
  {index: 0,
    err: 'Давай-ка не ври.',
    qType: 'text', // тип вопроса
    data: 'Здравствуй, %%truename%%! Сколько тебе лет? ', // сам вопрос
    options: {},
    answers:[ // все типы принимаемых ответов.
      {
        AnsType: 'text',  // тип ожидаемого ответа
        reg:/^([0-9]{1,2})$/i
      }
    ]
  },
  {index: 1,
    qType: 'text',
    data: 'Крутяк! Что ты хотел сделать, %%yearsOld%%-летний друг?', // сам вопрос
    options: BotHelper.makeButtons(['Узнать Время','Поиск', 'Погода', 'Динамически создаваемые кнопочки','Закончить'],[],1,false),
    answers:[
      {
        AnsType: 'text',
        reg:/^(Узнать Время)$/i,
        next: 2
      },
      {
        AnsType: 'text',
        reg:/^(Поиск)$/i,
        next: 3
      },
      {
        AnsType: 'text',
        reg:/^(Погода)$/i,
        next: 5
      },
      {
        AnsType: 'text',
        reg:/^(Закончить)$/i,
        next: 10
      },
      {
        AnsType: 'text',
        reg:/^(Динамически создаваемые кнопочки)$/i,
        next: 6
      }
    ]
  },
  {index: 2,
    qType: 'text', // тип вопроса
    data: 'Сейчас %%time%% ', // сам вопрос
    options: BotHelper.makeButtons(['Назад'],[],1,false),
    answers:[ // все типы принимаемых ответов.
      {
        AnsType: 'text',  // тип ожидаемого ответа
        reg:/(Назад)/i,
        next: 1
      }
    ]
  },
  {index: 3,
    qType: 'text', // тип вопроса
    data: 'Введи запрос.', // сам вопрос
    options: BotHelper.makeButtons(['Назад'],[],1,false),
    answers:[ // все типы принимаемых ответов.
      {
        AnsType: 'text',  // тип ожидаемого ответа
        reg:/(.*)/i
      },
      {
        AnsType: 'text',  // тип ожидаемого ответа
        reg:/(Назад)/i,
        next: 1
      }
    ]
  },
  {index: 4,
    qType: 'text', // тип вопроса
    data: '%%searchResult%%.', // сам вопрос
    options: BotHelper.makeButtons(['Назад'],[],1,false),
    answers:[ // все типы принимаемых ответов.
      {
        AnsType: 'text',  // тип ожидаемого ответа
        reg:/(Назад)/i,
        next: 1
      }
    ]
  },
  {index: 5,
    qType: 'text', // тип вопроса
    data: 'Сейчас %%weather%%.', // сам вопрос
    options: BotHelper.makeButtons(['Назад'],[],1,false),
    answers:[ // все типы принимаемых ответов.
      {
        AnsType: 'text',  // тип ожидаемого ответа
        reg:/(Назад)/i,
        next: 1
      }
    ]
  },
  {index: 6,
    qType: 'text', // тип вопроса
    data: 'Сколько кнопочек создать?', // сам вопрос
    options: BotHelper.makeButtons(['Назад'],[],1,false),
    answers:[ // все типы принимаемых ответов.
      {
        AnsType: 'text',  // тип ожидаемого ответа
        reg:/^([0-9]{1,2})$/i,
      },
      {
        AnsType: 'text',  // тип ожидаемого ответа
        reg:/(Назад)/i,
        next: 'prev'
      }
    ]
  },
  {index: 7,
    qType: 'text', // тип вопроса
    data: 'Сколько в ряд?', // сам вопрос
    options: BotHelper.makeButtons(['Назад'],[],1,false),
    answers:[ // все типы принимаемых ответов.
      {
        AnsType: 'text',  // тип ожидаемого ответа
        reg:/^([0-9])$/i,
      },
      {
        AnsType: 'text',  // тип ожидаемого ответа
        reg:/(Назад)/i,
        next: 'prev'
      }
    ]
  },
  {index: 8,
    qType: 'text', // тип вопроса
    data: 'С каким сообщением?', // сам вопрос
    options: BotHelper.makeButtons(['Назад'],[],1,false),
    answers:[ // все типы принимаемых ответов.
      {
        AnsType: 'text',  // тип ожидаемого ответа
        reg:/^(.*)$/i,
      },
      {
        AnsType: 'text',  // тип ожидаемого ответа
        reg:/^(Назад)$/i,
        next: 'prev'
      }
    ]
  },
  {index: 9,
    qType: 'text', // тип вопроса
    data: '[вы нажали: %%pressed%%] %%dynamicButtonsText%%', // сам вопрос
    options: {},
    answers:[ // все типы принимаемых ответов.
      {
        AnsType: 'inline',  // тип ожидаемого ответа
        reg:/^(.*)$/i,
        next: 9
      },
      {
        AnsType: 'inline',  // тип ожидаемого ответа
        reg:/(back)/i,
        next: 1
      },
      {
        AnsType: 'text',  // тип ожидаемого ответа
        reg:/^(Назад)$/i,
        next: 'prev'
      }
    ]
  },
]
module.exports = menu;
