var BotHelper = require('./Functions/BotFunctions');
const { bot, token } = require('./BotConnection');
console.log('Bot Started. Token: ', token);
var mongoose = require( 'mongoose' );
const { IndInObjArr } = require('./Functions/AllFunctions');
var options = require('./variables').options;

var google = require('google');
google.resultsPerPage = options.resultsPerPage;
google.lang = options.lang;
google.tld = options.tld;
var nextCounter = 0 // хз что это. От гугл поиска для примера.

//// test area start

  /* Send By Part Prototype. Короче говоря отправляет массив сообщений друг за другом. в конце вызывает callback
  // id - UID; parts - ['msg1', 'msg2'...]
  function sendByPart(id, parts, callback){
    var current = 0;
    function sendNext (next){
      if(current < parts.length-1){
        say(id, parts[next], function () {
          current++;
          sendNext(current);
        })
      } else {
        say(id, parts[next], function () {
          callback();
        })
      }
    }
    sendNext(current)
  }
  */

//// test area end

var menu = [
  ...require('./Menu/userMenu') // подключаете меню
]
var trigger = {
  startOn: { // условия при которых запускается опрос
    AnsType: 'any', // типа при любом смс запускается.
    reg: /(.*)/i
  },
  stopOn: { // услолвия при которых останавливается опрос
    force: [],    //  [{stop:false,uid:0}]
    AnsType: 'text',
    reg: /^\/stop/ // если человек написал /stop во время опроса,
  }
}

///  Functions Begin

function fmt(num){
  return (num < 10) ? '0'+num : num;
}
function getIndex(index) {
  return IndInObjArr(menu, index, 'index')[0]
}
function say(id, msg, callback, liveTime, opt){
  function dummy(msg){
    if(liveTime){
      setTimeout(function () {
        bot.deleteMessage(msg.chat.id, msg.message_id);
      }, liveTime);
    }
    if(callback)
    callback();
  }
  bot.sendMessage(id,msg||'...', opt).then(dummy).catch(callback||dummy)
}

function search(what, count, callback){
  google(what, function (err, res){
    if (err) callback(err)

    for (var i = 0; i < res.links.length; ++i) {
      var link = res.links[i];
      console.log('what', what, link.description );
      if(i < count){
        callback(link.description)
      }
    }

    if (nextCounter < 4) {
      nextCounter += 1
      if (res.next) res.next()
    }
  })
}

///////////////////////////
function firstCallBack (UID,username, msg, passed, session) {
  // Start callback
  session.thisGuy.truename = username ? '@'+username : msg.from.first_name;
  passed(true)
}

function midCallback (UID,answers,ind,passed,msg, session) {
  // Mid callback
  console.log(session);
  session.thisGuy.time = fmt(new Date().getHours())+':'+fmt(new Date().getMinutes());
  session.thisGuy.weather = 'Солнечно';
  session.thisGuy.yearsOld = answers[0];
  switch (ind) { // смотрим по индексу вопроса на который ответили
    case 3:
      if(answers[ind] == 'Назад'){
        passed(true);
        break;
      }
      search(answers[ind], 1, function (result) {
        session.thisGuy.searchResult = result;
        passed(true);
      })
      break;
    case 8: // перед тем как перейти к 9 пункту меню (когда ответили на 8), создаем кнопки.
      var index = getIndex(9) // берем индекс 8го вопроса (так как они могут быть не по порядку)
      var buttonsText = [];
      var buttonsData = [];
      for (var i = 0; i < answers[6]; i++) {
        buttonsText.push(i+'-я кнопка');
        buttonsData.push(i);
      }
      session.thisGuy.dynamicButtonsText = answers[8];
      menu[index].options = BotHelper.makeButtons([...buttonsText, 'В меню'], [...buttonsData, 'back'], answers[7], true);
      passed(true)
      break;
    case 9: // Когда нажимают на кнопочки на 9 пункте меню
      if(answers[ind] == 'back'){
        passed(true);
        break;
      }
      if(answers[ind] == 'Назад'){
        passed(true);
        break;
      }
      session.thisGuy.pressed = answers[ind];
      passed(true, 9)
      break;
    default:
    // остальные вопросы, которые не обратбатываются свитчем - будут проходить
    console.log(ind,answers[ind]);
    passed(true);
  }
}

////////////////////////////
function endCallBack (UID,answers,goBackTo,msg, session) {
  //
  goBackTo(false) // если надо чтобы он вернулся на какой-лдибо вопрос
  say(UID, 'Прощай... 😅 ')
  console.log(UID, 'Выпал из опроса!----------');
  console.log(answers);
  console.log('------Answers End------');
  console.log(msg);
  console.log('------Msg end------');
  console.log(session.thisGuy);
  console.log('------session end-------');
}
//////////////
function stopCallback(UID,force,answers) {
  bot.sendMessage(UID, 'Вы остановили Бота 😭')
}
// 4 параметр - колбэк
BotHelper.askFor(bot, menu, trigger,
  firstCallBack,
  midCallback,
  endCallBack,
  stopCallback,
  'MyBotSessions'
)
