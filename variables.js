const fs = require('fs');
const path = require('path');
const file = 'options.json'
const maxTryCount = 3;
var tryCount = 0;

var options = {
  masterPass: 'Говнёжка', // просто любые настройки по умолчанию
  resultsPerPage: 5, // настройки гугл поиска
  lang: 'ru',
  tld: 'ru',
  admins: [],
  load: loadOptions, // перед использованием настроек.
  save: saveOptions // вызывать каждый раз, как делаете изменения в настройках.
}

function save(fileName, content, callback) {
  fs.mkdir(path.join(__dirname, './options'), function(e) {
    var filePath = path.join(__dirname, './options/') + fileName;
    fs.writeFile(filePath, content, function(err) {
      if (err) {
        if(callback) callback(err, content, filePath)
        return;
      }
      if(callback) callback(null, content, filePath)
    });
  })
}

function read(fileName, callback) {
  var filePath = path.join(__dirname, './options/') + fileName;
  fs.readFile(filePath, 'utf8', function(err, data) {
    if (err) {
      if(callback) callback(err, null, null);
      return;
    }
    if(callback) callback(null, data, filePath);
  });
}



function saveOptions (callback){
  save(file, JSON.stringify( options ), callback)
}

function loadOptions( callback ) {
  read(file, function (err, loaded) {
    if(err&&tryCount <= maxTryCount) {
      console.log('Error loading options! Try №'+tryCount);
      tryCount++;
      saveOptions(function () {
        loadOptions( callback )
      });
      return;
    }
    if(tryCount <= maxTryCount){
      tryCount = 0;
      for (var key in JSON.parse(loaded)) {
        if (JSON.parse(loaded).hasOwnProperty(key)) {
          options[key] = JSON.parse(loaded)[key]
        }
      }
      console.log('Options Loaded try №', tryCount);
      if(callback) callback(loaded)
    }
  })
}

module.exports.options = options;
