var mongoose = require('mongoose');
mongoose.Promise = global.Promise

var Schema = mongoose.Schema;

var scheduleDay = new Schema({
    day: {type: Number},
    WeekSubjects: [
      {
        subject: {type: mongoose.Schema.Types.ObjectId, ref: 'Subject'},
        startTime: {type: String}, // '22:45'
        classRoom: {type: String, default:'хз где'},
        homeWork: {
          text: {type: String},
          photos: [{type: String}]
        },
        description : {type: String}
      }
    ]
},{
    timestamp:true
});

module.exports = mongoose.model('scheduleDay', scheduleDay);
