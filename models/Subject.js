var mongoose = require('mongoose');
mongoose.Promise = global.Promise

var Schema = mongoose.Schema;

var Subject = new Schema({
    name: {type: String, unique: true},
    teacher: {type: String, default:'хз кто'}
},{
    timestamp:true
});

module.exports = mongoose.model('Subject', Subject);
