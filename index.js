const mongoose = require('mongoose');
var options = require('./variables').options
mongoose.connect( 'mongodb://localhost:27017/testbase' );

var dbc = mongoose.connection;

dbc.once( 'open', function () {
  console.log( 'Mongoose Connected' );
  options.load(function () { // load options first
    require('./Bot.js');
  })
} );
